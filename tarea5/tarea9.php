<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Ejercicio9</title>
</head>
<body>
	<?php
		$resultado=1;
		while ($resultado != 0) {
			$nro = rand();
			$resultado =  $nro % 983;
		}
		echo "Un número divisible por 983 => ". number_format($nro, 0, ",", ".");
	?>
</body>
</html>