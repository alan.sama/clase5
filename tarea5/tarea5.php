<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<style>
		table, th, td {
		  border: 1px solid black;
		  border-collapse: collapse;
		}
		td {
			text-align: center;
		}
		#tabla tr:nth-child(even) {
			background-color: lightgrey;
		}
	</style>

	<title>Ejercicio5</title>
</head>
<body>
	<table id="tabla">
			<tr>
				<th>Multiplicación</th>
				<th>Resultado</th>
			</tr>
			<?php
				for ($i = 0; $i<= 10; $i++) {
					echo "<tr>";
					echo "<td>".$i." * 9 </td>";
					echo "<td>".$i * 9 ."</td>";
					echo "</tr>";
				}
			?>
	</table>
</body>
</html>